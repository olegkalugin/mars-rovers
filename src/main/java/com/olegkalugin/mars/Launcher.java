package com.olegkalugin.mars;

import com.olegkalugin.mars.controller.Navigator;
import com.olegkalugin.mars.exception.RoverDestroyedException;

import java.io.IOException;

public class Launcher {

    public static void main(String... args) throws IOException, RoverDestroyedException {
        Navigator navigator = new Navigator();

        DataReader dataReader = new DataReader();
        dataReader.readData(navigator);

        navigator.launch();
    }
}
