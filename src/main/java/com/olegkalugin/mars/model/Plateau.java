package com.olegkalugin.mars.model;

public class Plateau {
    private int width;
    private int height;

    public Plateau(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public boolean isCoordinateInside(int x, int y) {
        return x >= 0 && x <= width && y >= 0 && y <= height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
