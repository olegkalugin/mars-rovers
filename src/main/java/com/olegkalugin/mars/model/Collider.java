package com.olegkalugin.mars.model;

public interface Collider {
    boolean collidesWith(Collider otherCollider);

    int getX();

    int getY();
}
