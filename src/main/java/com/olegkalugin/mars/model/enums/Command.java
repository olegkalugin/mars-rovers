package com.olegkalugin.mars.model.enums;

public enum Command {
    LEFT("L"), RIGHT("R"), MOVE("M");

    private final String value;

    private Command(String value) {
        this.value = value;
    }

    public static Command getByValue(String value) {
        for (Command command : values()) {
            if (value.equals(command.value)) {
                return command;
            }
        }
        return null;
    }
}
