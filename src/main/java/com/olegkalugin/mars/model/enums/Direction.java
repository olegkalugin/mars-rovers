package com.olegkalugin.mars.model.enums;

public enum Direction {
    N(0, 1), E(1, 0), S(0, -1), W(-1, 0);

    private final int x;
    private final int y;

    private Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Direction getNext() {
        return values()[(ordinal() + 1) % values().length];
    }

    public Direction getPrevious() {
        return values()[(ordinal() - 1 + values().length) % values().length];
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
