package com.olegkalugin.mars.model;

import com.olegkalugin.mars.model.enums.Direction;

public class Rover implements Collider {
    private int x;
    private int y;
    private Direction direction;

    public Rover(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    @Override
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public boolean collidesWith(Collider otherCollider) {
        return x == otherCollider.getX() && y == otherCollider.getY();
    }
}
