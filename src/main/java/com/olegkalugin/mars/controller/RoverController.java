package com.olegkalugin.mars.controller;

import com.olegkalugin.mars.exception.RoverDestroyedException;
import com.olegkalugin.mars.model.Plateau;
import com.olegkalugin.mars.model.Rover;
import com.olegkalugin.mars.model.enums.Command;
import com.olegkalugin.mars.model.enums.Direction;

public class RoverController {
    private Rover rover;
    private final Plateau plateau;

    public RoverController(Plateau plateau) {
        this.plateau = plateau;
    }

    public void executeCommand(Command command) throws RoverDestroyedException {
        switch (command) {
            case LEFT:
                rover.setDirection(rover.getDirection().getPrevious());
                break;
            case RIGHT:
                rover.setDirection(rover.getDirection().getNext());
                break;
            case MOVE:
                moveRover();
                break;
        }
    }

    private void moveRover() throws RoverDestroyedException {
        Direction direction = rover.getDirection();

        int newX = rover.getX() + direction.getX();
        validateCoordinate(newX, plateau.getWidth());
        rover.setX(newX);

        int newY = rover.getY() + direction.getY();
        validateCoordinate(newY, plateau.getHeight());
        rover.setY(newY);
    }

    private void validateCoordinate(int coordinate, int boundary) throws RoverDestroyedException {
        if (coordinate < 0 || coordinate > boundary) {
            rover = null;
            throw new RoverDestroyedException("Congratulations! You just drove a Mars rover off of the plateau.");
        }
    }

    public void setRover(Rover rover) {
        this.rover = rover;
    }
}
