package com.olegkalugin.mars.controller;

import com.olegkalugin.mars.exception.RoverDestroyedException;
import com.olegkalugin.mars.model.Plateau;
import com.olegkalugin.mars.model.Rover;
import com.olegkalugin.mars.model.enums.Command;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Navigator {

    private Plateau plateau;
    private Map<Rover, String> roverCommands = new LinkedHashMap<Rover, String>();
    private RoverController roverController;

    public void launch() throws RoverDestroyedException {
        roverController = new RoverController(plateau);

        for (Map.Entry<Rover, String> entry : roverCommands.entrySet()) {
            Rover rover = entry.getKey();
            roverController.setRover(rover);

            char[] commands = entry.getValue().toCharArray();
            executeCommands(rover, commands);

            System.out.print(rover.getX() + " " + rover.getY() + " " + rover.getDirection() + System.lineSeparator());
        }
    }

    private void executeCommands(Rover rover, char[] commands) throws RoverDestroyedException {
        for (char commandCode : commands) {
            Command command = Command.getByValue(String.valueOf(commandCode));
            roverController.executeCommand(command);
            checkForCollisions(rover);
        }
    }

    private void checkForCollisions(Rover rover) throws RoverDestroyedException {
        for (Rover otherRover : roverCommands.keySet()) {
            if (rover != otherRover && rover.collidesWith(otherRover)) {
                throw new RoverDestroyedException("Congratulations! You just collided two Mars rovers.");
            }
        }
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public void addRoverCommands(Rover rover, String commands) {
        if (roverCommands == null) {
            roverCommands = new LinkedHashMap<Rover, String>();
        }

        if (plateau.isCoordinateInside(rover.getX(), rover.getY())) {
            roverCommands.put(rover, commands);
        }
    }

    public Set<Rover> getRovers() {
        return roverCommands.keySet();
    }
}
