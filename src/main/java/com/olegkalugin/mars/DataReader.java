package com.olegkalugin.mars;

import com.olegkalugin.mars.controller.Navigator;
import com.olegkalugin.mars.model.Plateau;
import com.olegkalugin.mars.model.Rover;
import com.olegkalugin.mars.model.enums.Direction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DataReader {

    public void readData(Navigator navigator) throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(Navigator.class.getResourceAsStream("/input.txt")));

        navigator.setPlateau(readPlateau(br));

        String roverCoordinates;
        while ((roverCoordinates = br.readLine()) != null) {
            Rover rover = readRover(roverCoordinates);

            String commands = br.readLine();
            navigator.addRoverCommands(rover, commands);
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Plateau readPlateau(BufferedReader br) throws IOException {
        String[] plateauArgs = br.readLine().split(" ");
        return new Plateau(Integer.valueOf(plateauArgs[0]), Integer.valueOf(plateauArgs[1]));
    }

    private Rover readRover(String roverCoordinates) {
        String[] coordinateArgs = roverCoordinates.split(" ");

        return new Rover(Integer.valueOf(coordinateArgs[0]),
                Integer.valueOf(coordinateArgs[1]),
                Direction.valueOf(coordinateArgs[2]));
    }
}
