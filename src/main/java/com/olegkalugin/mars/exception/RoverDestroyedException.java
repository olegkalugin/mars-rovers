package com.olegkalugin.mars.exception;

public class RoverDestroyedException extends Exception {
    public RoverDestroyedException(String message) {
        super(message);
    }
}
