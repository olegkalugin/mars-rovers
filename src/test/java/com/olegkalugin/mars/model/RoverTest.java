package com.olegkalugin.mars.model;

import com.olegkalugin.mars.model.enums.Direction;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RoverTest {

    private final Rover mainRover = new Rover(1, 1, Direction.N);

    @Test
    public void testCollidesWith() throws Exception {
        Rover secondaryRover = new Rover(1, 1, Direction.N);
        assertTrue(mainRover.collidesWith(secondaryRover));
    }

    @Test
    public void testDoesNotCollide() throws Exception {
        Rover secondaryRover = new Rover(1, 2, Direction.N);
        assertFalse(mainRover.collidesWith(secondaryRover));
    }
}