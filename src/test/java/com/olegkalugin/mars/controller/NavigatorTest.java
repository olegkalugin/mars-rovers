package com.olegkalugin.mars.controller;

import com.olegkalugin.mars.exception.RoverDestroyedException;
import com.olegkalugin.mars.model.Plateau;
import com.olegkalugin.mars.model.Rover;
import com.olegkalugin.mars.model.enums.Direction;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class NavigatorTest {

    private Navigator navigator;
    private final Rover rover1 = new Rover(1, 2, Direction.N);
    private final Rover rover2 = new Rover(3, 3, Direction.E);

    @Before
    public void setUp() throws Exception {
        navigator = new Navigator();
        navigator.setPlateau(new Plateau(5, 5));
    }

    @Test
    public void testHappyLaunch() throws Exception {
        navigator.addRoverCommands(rover1, "LMLMLMLMM");
        navigator.addRoverCommands(rover2, "MMRMMRMRRM");

        navigator.launch();

        Set<Rover> rovers = navigator.getRovers();
        Iterator<Rover> iterator = rovers.iterator();

        Rover testRover1 = iterator.next();
        assertEquals(1, testRover1.getX());
        assertEquals(3, testRover1.getY());
        assertEquals(Direction.N, testRover1.getDirection());

        Rover testRover2 = iterator.next();
        assertEquals(5, testRover2.getX());
        assertEquals(1, testRover2.getY());
        assertEquals(Direction.E, testRover2.getDirection());
    }

    @Test(expected = RoverDestroyedException.class)
    public void testDriveOffBounds() throws Exception {
        navigator.addRoverCommands(rover1, "MMMMMMMMM");
        navigator.launch();
    }

    @Test(expected = RoverDestroyedException.class)
    public void testCollision() throws Exception {
        navigator.addRoverCommands(rover1, "M");
        navigator.addRoverCommands(rover2, "RRMM");
        navigator.launch();
    }
}